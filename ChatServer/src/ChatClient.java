import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.Socket;
import java.net.UnknownHostException;


/**
 * 
 * Chat Client Version 0
 * Works with ChatServer Versions 0,1,2,3 (does not work with 4,5 because those use threaded clients)
 * server 0 accepts one response before quiting
 * server 1 accepts one client, handles responses until the client types "bye", then quits
 * server 2 accepts one client at a time until it says bye, and then handles the next queued client
 * server 3 handles multiple clients simultaneously. clients each have single chat communication with the server
 * server 4 handles multiple clients simultaneously. All connected clients an see all other clients comments (group chat)
 * server 5 handles multiple clients simultaneously. Group and Private Chat is available between connected clients
 * 
 */
public class ChatClient {

	private Socket socket = null;
	BufferedReader console = null;
	private DataOutputStream  streamOut = null;
	
	public ChatClient(String serverName, int serverPort){
		try{
		socket = new Socket(serverName, serverPort);//connect to a server using their known port
		System.out.println("Got connected to socket : "+socket+ " on port "+serverPort);
		start(); //we're streamin...
		String line = "";
		while(!line.equalsIgnoreCase("bye")){
			line = console.readLine(); //read the input
			streamOut.writeUTF(line);
			streamOut.flush();
		}
		}catch(UnknownHostException uhe){
			System.out.println("Unknown Host: OOPS! "+ uhe.getMessage());
		}
		catch(IOException ioe){
			System.out.println("IO problem client: "+ioe.getMessage());
		}
		
		
	}
	
	
	public void start()throws IOException{
		console = new BufferedReader(new InputStreamReader(System.in));
		streamOut = new DataOutputStream(socket.getOutputStream());
	}
	
	
	public static void main(String [] args){
		//ChatClient client = new ChatClient("localhost", 8081);
		
		ChatClient client = null;
		if(args.length !=2){
			System.out.println("To chat specify the servername and the port");
		}else{
			client = new ChatClient(args[0], Integer.parseInt(args[1]));
		}
		
	}
	
	
	
}
