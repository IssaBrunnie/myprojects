import java.io.DataInputStream;
import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
/**
 * 	Version 3: 
 *  works with the same client as before (client version 0)
 *  Server  will remain 'open' for additional connection once a client has quit. 
 *  Server can handle multiple clients simultaneously. 
 *  The output from all connected clients will appear on the server's screen.
 *  The following are the same as the previous version: default constructor, start method, main method
 * */

/**
 * * server 0 accepts one response before quiting
 * server 1 accepts one client, handles responses until the client types "bye", then quits
 * server 2 accepts one client at a time until it says bye, and then handles the next queued client
 * server 3 handles multiple clients simultaneously. clients each have single chat communication with the server
 * server 4 handles multiple clients simultaneously. All connected clients an see all other clients comments (group chat)
 * server 5 handles multiple clients simultaneously. Group and Private Chat is available between connected clients
 * 
 */


public class ChatServer implements Runnable{

	private ServerSocket server = null;
	private ChatServerThread client = null;
	//private Socket socket =  null;
	//private DataInputStream streamIn = null;
	private Thread thread = null;
	private boolean done = false;// will change this
	
	//same as previous version
	public ChatServer(int port){
		try{
			System.out.println("Binding to port "+port+ "..wait !!!");
			server = new ServerSocket(port);
			start();
		}catch(IOException ioe){
			System.out.println("OOPS! : " + ioe.getMessage());
		}
	}
	//same as previous version
	public static void main(String[] args){
		//ChatServer server = new ChatServer(8081);
		
		ChatServer server = null;
		if(args.length !=1){
			System.out.println("To run a chat server you need to specify a port");
		}else{
			server = new ChatServer(Integer.parseInt(args[0]));//first argument is port
		}
		
	}
	//same as previous version
	public void start(){
		if(thread ==null ){
			thread = new Thread(this);
			thread.start();// will call the thread's run method
		}
	}
	//same as previous version
	public void stop(){
		if(thread !=null){
			thread=null;
			done=true; // we don't say thread.stop anymore.. that is deprecated
		}
	}

	public void addThread(Socket socket){
		//create a new ChatServerThread client using the constructor
		client = new ChatServerThread(this, socket);
		//try to open the stream
		try{
			client.open();
			client.start();
		}catch(IOException ioe){
			System.out.println("OOPS! tried to add thread client" + ioe.getMessage());
		}
		//try to start running the ChatServerThread client
	}
	@Override
	public void run() {
		while(thread!=null){
			try{
				System.out.println("Waiting for a client.... inside run");
				addThread(server.accept()); 
				
			}catch(IOException ioe){
				System.out.println("OOPSY" + ioe.getMessage());
			}
		}
	}
	
}

