import java.io.BufferedInputStream;
import java.io.DataInputStream;
import java.io.IOException;
import java.net.Socket;
/**
 * 
 * Separate Thread Class used by ChatServer Version3
 * handles each incoming Chat on a separate thread
 *
 */
public class ChatServerThread extends Thread {
	private Socket socket = null;
	private ChatServer server = null;
	private DataInputStream streamIn = null;
	boolean done = true;
	private int ID = -1;
	
	public ChatServerThread(ChatServer _server, Socket _socket){
		//assign the values
		server = _server;
		socket = _socket;
		ID = socket.getPort();
		System.out.println("Chat Server Thread Info: SERVER: " +
									server +"SOCKET: "+socket+" ID:"+ID);
	
	}
	
	public void run(){
		try{
			while( ID!= -1){//while client has valid ID
				getInput();//get input from the client
				close();// close up the stream and socket
			}
		}catch(IOException ioe){
			System.out.println("Thread Client Issue" + ioe.getMessage());
		}
	}
	
	public void getInput(){
		done = false;// set the flag to false since the method got called
		while(!done){
			try{
				String line = streamIn.readUTF();
				System.out.println("Message from " +ID+": "+line);
				done = line.equalsIgnoreCase("bye");
				
			}catch(IOException ioe){
				System.out.println("Input issue" + ioe.getMessage());
			}
		}
		System.out.println(ID + " left the chat...");
		ID = -1; //reset ID to invalid value
	}

	public void open() throws IOException{
		streamIn = new DataInputStream(new BufferedInputStream(socket.getInputStream()));
	}
	
	public void close () throws IOException{
		
	}
}

