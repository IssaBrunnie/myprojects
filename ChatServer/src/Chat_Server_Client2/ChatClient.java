package Chat_Server_Client2;
/*
 * chat client version 4(0,1,2,3 all used version 0)
 * send and receive message unlike the previous version
 * It creates a separate thread to handle communications with/from the server
 */

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.Socket;
import java.net.UnknownHostException;

public class ChatClient implements Runnable{
	
	private Socket socket = null;
	BufferedReader console = null;
	private DataOutputStream  streamOut = null;
	private ChatClientThread client = null;
	private boolean done = false;
	private String line = "";
	private Thread thread = null;

	public ChatClient(String serverName, int serverPort){
		try{
		socket = new Socket(serverName, serverPort);//connect to a server using their known port
		System.out.println("Got connected to socket : "+socket+ " on port "+serverPort);
		start(); //we're streamin...
		String line = "";
		while(!line.equalsIgnoreCase("bye")){
			line = console.readLine(); //read the input
			streamOut.writeUTF(line);
			streamOut.flush();
		}
		}catch(UnknownHostException uhe){
			System.out.println("Unknown Host: OOPS! "+ uhe.getMessage());
		}
		catch(IOException ioe){
			System.out.println("IO problem client: "+ioe.getMessage());
		}
		
	}
	
	public void start()throws IOException{
		console = new BufferedReader(new InputStreamReader(System.in));
		streamOut = new DataOutputStream(socket.getOutputStream());
		if(thread == null){
			client = new ChatClientThread(this, socket);
			thread = new Thread(this);
			thread.start();
		}
	}
	
	public void stop(){
		done = true; //set flag to done
		if(thread!=null){
			thread=null;
		}try{
			if(console != null)console.close();
			if(streamOut != null)streamOut.close();
			if(client!=null)client=null;
			if(socket!=null)socket.close();
		}catch(IOException ioe){System.out.println("Error closing..." + ioe.getMessage());}
	}
	
	@Override
	public void run() {
		while((thread != null) && ( !line.equalsIgnoreCase("bye"))){
			try{
				line = console.readLine();
				streamOut.writeUTF(line);
				streamOut.flush();
			}catch(IOException ioe){
				System.out.println("Error sending the message" + ioe.getMessage());
			}
		}
	}
	
	public void handle(String msg){
		if(msg.equalsIgnoreCase("bye")){
			line = "bye";
			stop();
		}
		else{
			System.out.println(msg);
		}
	}
}
