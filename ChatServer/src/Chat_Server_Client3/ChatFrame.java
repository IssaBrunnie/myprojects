package Chat_Server_Client3;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Font;
import java.awt.GridLayout;
import java.awt.Label;
import java.awt.TextArea;
import java.awt.TextField;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.Socket;
import java.net.UnknownHostException;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;

public class ChatFrame extends JFrame implements ActionListener,Runnable{
	
	private Socket socket			= null;
	private DataInputStream streamIn = null;
	private DataOutputStream streamOut = null;
	private ChatClientThread client = null;
	private TextArea display 		= new TextArea();
	private TextField input 		= new TextField();
	//private TextField prvMsg		= new TextField();
	//private JButton pBtn			= new JButton("Private");
	private JButton send			= new JButton("Send");
	private JButton connect			= new JButton("Connect");
	private JButton quit			= new JButton("Bye");
	private String serverName		= "localhost"; //will get from browser
	private int	   serverPort 		= 8081;//will get from browser
	static JPanel mainPanel = new JPanel();
	static JPanel keys 		= new JPanel();
	static JPanel south		= new JPanel();
	static JPanel Dm		= new JPanel();
	private boolean done = true;
	private String line = "";
	
	private ChatServerThread clients[] = new ChatServerThread[5];
	private int clientCount = 0;
	
	public static void main(String [] args){
		new ChatFrame().setVisible(true);
	}
	
	public ChatFrame(){
		super("Chatter ");
		setSize(800,370);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
		mainPanel.setLayout(new BorderLayout());
		keys.setLayout(new GridLayout(1,2));
		connect.setEnabled(true);
		connect.addActionListener(this);
		quit.setEnabled(false);
		quit.addActionListener(this);
		keys.add(quit);
		keys.add(connect);
		
		Label intro = new Label("For Private Msg \n type 'DM:' plus ID on textfield ^^^ Above ^^^.");
		intro.setFont(new Font("Times New Roman",Font.CENTER_BASELINE,14));
		Dm.setLayout(new BorderLayout());
		Dm.add("Center", intro);
		/*Dm.add("Center", prvMsg);
		Dm.add("East", pBtn);
		pBtn.setEnabled(false);
		pBtn.addActionListener(this);*/
		
		south.setLayout(new BorderLayout());
		south.add("West", keys);
		south.add("Center", input);
		south.add("South", Dm);
		send.setEnabled(false);
		send.addActionListener(this);
		south.add("East", send);
		
		Label title = new Label("Welcome to Chatter", Label.CENTER);
		title.setFont(new Font("Times New Roman", Font.BOLD, 14));
		
		mainPanel.add(title, BorderLayout.NORTH);
		display.setEditable(false);
		display.setBackground(Color.GREEN);
		mainPanel.add(display, BorderLayout.CENTER);
		mainPanel.add(south, BorderLayout.SOUTH);
		add(mainPanel);
	}

	public void open(){
		try{
			streamOut = new DataOutputStream(socket.getOutputStream());
			streamIn = new DataInputStream(socket.getInputStream());
			new Thread(this).start();
		}catch(IOException ioe){
			ioe.printStackTrace();
		}
	}

	@Override
	public void run() {
		// TODO Auto-generated method stub
		//while !done... read in the input... display line
		try{
			while(!done){
				line = streamIn.readUTF();
				displayOutput(line);
			}
		}catch(IOException ioe){
			done = true;
			displayOutput(ioe.getMessage());
		}
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		// TODO Auto-generated method stub
		if(e.getSource()==connect){
			connect(serverName, serverPort);
			System.out.println("connect button hit");
		}
		else if(e.getSource()==quit){
			disconnect();
			System.out.println("quit button hit");
		}
		else if(e.getSource()==send){
			send();
			System.out.println("send button hit");
		}
		/*else if(e.getSource()== pBtn){
			diem();
			System.out.println("private button hit");
		}*/
	}
	
	public void connect(String serverName, int serverPort){
		//connect to the server
		done = false;
		displayOutput("Connection Time...");
		try{
			socket = new Socket(serverName, serverPort);
			displayOutput("Connected: " + socket);
			open();
			send.setEnabled(true);//enable the send button
			quit.setEnabled(true);//enable the quit button
			connect.setEnabled(false);//cant connect to same server
			//pBtn.setEnabled(true);
			
		}catch(UnknownHostException uhe){
			displayOutput(uhe.getMessage());
			done=true;
		}
		catch(IOException ioe){
			displayOutput(ioe.getMessage());
			done = true;	
		}
	}
	public void disconnect(){
		//disconnect from server...set flags
		done = true;
		input.setText("bye");
		send();
		quit.setEnabled(false);
		connect.setEnabled(true);
		send.setEnabled(false);
		//pBtn.setEnabled(false);
	}
	
	public void send(){
		//get text from the input
		String msg = input.getText().trim();
		//try and send the information... set text in the gui
		try{
			streamOut.writeUTF(msg);
			streamOut.flush();
			input.setText("");
			if(msg.equalsIgnoreCase("bye")){
				quit.setEnabled(false);
				connect.setEnabled(true);
				send.setEnabled(false);
				//pBtn.setEnabled(false);
				close();
			}
		}catch(IOException ioe){
			displayOutput("Problem sending..."+ ioe.getMessage());
			close();
		}
	}
	/*public void diem(){
		//private msg
		String prvt = prvMsg.getText().trim();
		int ID_SendTO = Integer.parseInt(prvt.substring(prvt.length(),prvt.length()+5));
		String Pmsg = prvt.substring(prvt.length()+6);
		if(findClient(ID_SendTO)!=-1){
			clients[findClient(ID_SendTO)].send(Pmsg + "from " + "ID");
		}
	}*/
	
	public void handle(String msg){
		//how do we display the message?
		displayOutput(msg);
		if(msg.equalsIgnoreCase("bye")){
			displayOutput("GOODBYE!!!");
			close();
		}
	}
	public void displayOutput(String msg){
		//append to the display
		display.append("\n" + msg + "\n------");
	}
	public void close(){
		done = true;
		try{
			if(streamOut != null){ streamOut.close();}
			if(socket != null){ socket.close();}
		}catch(IOException ioe){
			displayOutput("OPPPS problem closing...");
			client.close();
			client=null;
		}
	}
	private int findClient(int ID){
		for(int i=0; i<clientCount; i++){
			if(clients[i].getID() == ID){
				return i;
			}
		}
		return -1;
	}
}	