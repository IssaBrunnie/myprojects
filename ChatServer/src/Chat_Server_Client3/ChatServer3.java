package Chat_Server_Client3;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;

public class ChatServer3 implements Runnable{
	
	private ServerSocket server = null;
	private ChatServerThread clients[] = new ChatServerThread[5]; //change
	private Thread thread = null;
	private int clientCount = 0;
	
	public ChatServer3(int port){
		try{
			System.out.println("Binding to port "+port+ "..wait !!!");
			server = new ServerSocket(port);
			start();
		}catch(IOException ioe){
			System.out.println("OOPS! : " + ioe.getMessage());
		}
	}
	
	public void EarlyBird(){
		if(clientCount >= 5){
			for(int i=0;i<3;i++){
				clients[i].send("CONGRATULATION!!!!You won a free SLURPEE from 7/11 for joining Chatter.");
			}
		}
	}
	//same as previous version
	public static void main(String[] args){
		//ChatServer server = new ChatServer(8081);
		ChatServer3 server = null;
		if(args.length !=1){
			System.out.println("To run a chat server you need to specify a port");
		}else{
			server = new ChatServer3(Integer.parseInt(args[0]));//first argument is port
		}
		
	}
	//same as previous version
		public void start(){
			if(thread ==null ){
				thread = new Thread(this);
				thread.start();// will call the thread's run method
			}
		}
		//same as previous version
		public void stop(){
			if(thread !=null){
				thread=null;
			}
		}
	
		public synchronized void handle(int ID, String input){
			String privMsg = "DM:";
			if(input.startsWith(privMsg)){
				int ID_SendTO = Integer.parseInt(input.substring(privMsg.length(),privMsg.length()+5));
				String msg = input.substring(privMsg.length()+6);
				if(findClient(ID_SendTO)!=1){
					clients[findClient(ID_SendTO)].send(msg+"Message from User: "+ID);
				}
			}
			else{
				for(int i=0; i<clientCount; i++){
					clients[i].send("USER: " + ID + "  said  " + input);
				}
			}
			if(input.equalsIgnoreCase("bye")){
				remove(ID);
			}
		}
		
		public synchronized void remove(int ID){
			int pos = findClient(ID); //need to create the method
			if(pos >= 0){
				ChatServerThread toTerminate = clients[pos];
				System.out.println("Removing client thread" + ID + " at " + pos);
				if(pos < clientCount -1){
					for(int i = pos+1; i < clientCount; i++){
						clients[i-1] = clients[i];
					}
					clientCount --;
				}
				try{
					toTerminate.close();
				}catch(IOException ioe){
					System.out.println("Error closing the thread " + ioe.getMessage());
				}
			}
		}
		
		private int findClient(int ID){
			for(int i=0; i<clientCount; i++){
				if(clients[i].getID() == ID){
					return i;
				}
			}
			return -1;
		}
		private synchronized void addThread(Socket socket){
			//create a new ChatServerThread client using the constructor
			if (clientCount < clients.length){
				clients[clientCount]= new ChatServerThread(this, socket);
			
			//client = new ChatServerThread(this, socket);
			//try to open the stream
				try{
					clients[clientCount].open();
					clients[clientCount].start();
					clientCount++;
				}catch(IOException ioe){
					System.out.println("OOPS! tried to add thread client" + ioe.getMessage());
				}
			}
		}
		
	@Override
	public void run() {
		// TODO Auto-generated method stub
		
		while(thread!=null){
			try{
				System.out.println("Waiting for a client.... inside run");
				addThread(server.accept());
				EarlyBird(); 
				
			}catch(IOException ioe){
				System.out.println("OOPSY" + ioe.getMessage());
			}
		}
	}
}


