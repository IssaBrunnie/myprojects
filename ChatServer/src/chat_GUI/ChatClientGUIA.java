package chat_GUI;

import java.applet.Applet;
import java.awt.BorderLayout;
import java.awt.Button;
import java.awt.Color;
import java.awt.Font;
import java.awt.GridLayout;
import java.awt.Label;
import java.awt.Panel;
import java.awt.TextArea;
import java.awt.TextField;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.Socket;
import java.net.UnknownHostException;

import Chat_Server_Client3.ChatClientThread;

public class ChatClientGUIA extends Applet implements ActionListener, Runnable{
	private Socket socket			= null;
	private DataInputStream streamIn = null;
	private DataOutputStream streamOut = null;
	private ChatClientThread client = null;
	private TextArea display 		= new TextArea();
	private TextField input 		= new TextField();
	private Button send				= new Button("Send");
	private Button connect			= new Button("Connect");
	private Button quit				= new Button("Bye");
	private String serverName		= "localhost"; //will get from browser
	private int	   serverPort 		= 8081;//will get from browser
	Panel mainPanel = new Panel();
	Panel keys 		= new Panel();
	Panel south		= new Panel();
	private boolean done = true;
	private String line = "";
	
	public void init(){
		//set up gui...
		mainPanel.setLayout(new BorderLayout());
		keys.setLayout(new GridLayout(1,2));
		connect.setEnabled(true);
		connect.addActionListener(this);
		quit.setEnabled(false);
		quit.addActionListener(this);
		keys.add(quit);
		keys.add(connect);
		
		south.setLayout(new BorderLayout());
		south.add("West", keys);
		south.add("Center", input);
		send.setEnabled(false);
		send.addActionListener(this);
		south.add("East", send);
		
		Label title = new Label("Our lovely chat", Label.CENTER);
		title.setFont(new Font("Helvetica", Font.BOLD, 14));
		
		mainPanel.add(title, BorderLayout.NORTH);
		display.setEditable(false);
		display.setBackground(Color.GREEN);
		mainPanel.add(display, BorderLayout.CENTER);
		mainPanel.add(south, BorderLayout.SOUTH);
		add(mainPanel);
	}
	
	public void open(){
		try{
			streamOut = new DataOutputStream(socket.getOutputStream());
			streamIn = new DataInputStream(socket.getInputStream());
			new Thread(this).start();
		}catch(IOException ioe){
			ioe.printStackTrace();
		}
	}

	@Override
	public void run() {
		// TODO Auto-generated method stub
		//while !done... read in the input... display line
		try{
			while(!done){
				line = streamIn.readUTF();
				displayOutput(line);
			}
		}catch(IOException ioe){
			done = true;
			displayOutput(ioe.getMessage());
		}
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		// TODO Auto-generated method stub
		if(e.getSource()==connect){
			connect(serverName, serverPort);
			System.out.println("connect button hit");
		}
		else if(e.getSource()==quit){
			disconnect();
			System.out.println("quit button hit");
		}
		else if(e.getSource()==send){
			send();
			System.out.println("send button hit");
		}
	}
	
	public void connect(String serverName, int serverPort){
		//connect to the server
		done = false;
		displayOutput("Connection Time...");
		try{
			socket = new Socket(serverName, serverPort);
			displayOutput("Connected: " + socket);
			open();
			send.setEnabled(true);//enable the send button
			quit.setEnabled(true);//enable the quit button
			connect.setEnabled(false);//cant connect to same server
			
		}catch(UnknownHostException uhe){
			displayOutput(uhe.getMessage());
			done=true;
		}
		catch(IOException ioe){
			displayOutput(ioe.getMessage());
			done = true;	
		}
	}
	public void disconnect(){
		//disconnect from server...set flags
		done = true;
		input.setText("bye");
		send();
		quit.setEnabled(false);
		connect.setEnabled(true);
		send.setEnabled(false);
	}
	
	public void send(){
		//get text from the input
		String msg = input.getText().trim();
		//try and send the information... set text in the gui
		try{
			displayOutput(msg);
			streamOut.writeUTF(msg);
			streamOut.flush();
			if(msg.equalsIgnoreCase("bye")){
				quit.setEnabled(false);
				connect.setEnabled(true);
				send.setEnabled(false);
				close();
			}
		}catch(IOException ioe){
			displayOutput("Problem sending..."+ ioe.getMessage());
			close();
		}
	}
	
	public void handle(String msg){
		//how do we display the message?
		if(msg.equalsIgnoreCase("bye")){
			displayOutput("GOODBYE !");
			close();
		}
	}
	public void displayOutput(String msg){
		//append to the display
		display.append("\n" + msg + "\n------");
	}
	public void close(){
		done = true;
		try{
			if(streamOut != null){ streamOut.close();}
			if(socket != null){ socket.close();}
		}catch(IOException ioe){
			displayOutput("OPPPS problem closing...");
			client.close();
			client=null;
		}
	}
}
