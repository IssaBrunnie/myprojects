import java.awt.Color;
import java.awt.Graphics;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;

import javax.swing.JPanel;

public class DrawingBoard extends JPanel implements MouseListener,MouseMotionListener{

	private int lastX=0, lastY=0;
	private Color shapeColor = Color.BLUE;
	private int shapeSize = 25;
	
	public void record(int x, int y){
		lastX=x;
		lastY=y;
		
	}
	@Override
	public void mouseClicked(MouseEvent e) {
		// TODO Auto-generated method stub
		int x = e.getX();
		int y = e.getY();
		record(x,y);
	}

	@Override
	public void mouseEntered(MouseEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void mouseExited(MouseEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void mousePressed(MouseEvent e) {
		// TODO Auto-generated method stub
		record(e.getX(), e.getY());
		Graphics g = getGraphics();
		g.setColor(shapeColor);
		g.fillOval(lastX-shapeSize, lastY-shapeSize, 2*shapeSize, 2*shapeSize);
	}

	@Override
	public void mouseReleased(MouseEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void mouseDragged(MouseEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void mouseMoved(MouseEvent e) {
		// TODO Auto-generated method stub
		
	}

}
