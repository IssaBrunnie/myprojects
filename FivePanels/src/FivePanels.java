import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JApplet;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;

public class FivePanels extends JApplet 
							implements ActionListener 
{
	JPanel jpN = new JPanel();
	JPanel jpS = new JPanel();
	JPanel jpE = new JPanel();
	JPanel jpW = new JPanel();
	JPanel jpC = new JPanel();
	
	@Override
	public void init()
	{
		jpN.setBackground(Color.WHITE);
		jpN.add(new JLabel("North"));
		add(jpN,BorderLayout.NORTH);
		
		jpS.setBackground(Color.WHITE);
		jpS.add(new JLabel("South"));
		add(jpS,BorderLayout.SOUTH);
		
		jpE.setBackground(Color.WHITE);
		jpE.add(new JLabel("East"));
		add(jpE,BorderLayout.EAST);
		
		jpW.setBackground(Color.WHITE);
		jpW.add(new JLabel("West"));
		add(jpW,BorderLayout.WEST);
		
		jpC.setBackground(Color.WHITE);
		jpC.add(new JLabel("Center"));
		add(jpC,BorderLayout.CENTER);
		jpC.addMouseListener(new CircleDrawerAdpter());
		
		JButton NorB = new JButton("North Button");
		JButton SouB = new JButton("South Button");
		JButton EasB = new JButton("East Button");
		JButton WesB = new JButton("West Button");
		
		jpN.add(NorB,BorderLayout.NORTH);
		NorB.addActionListener(this);
		
		jpS.add(SouB,BorderLayout.SOUTH);
		SouB.addActionListener(this);
		
		jpE.add(EasB,BorderLayout.EAST);
		EasB.addActionListener(this);
		
		jpW.add(WesB,BorderLayout.WEST);
		WesB.addActionListener(this);
		
	}
	
	public void actionPerformed(ActionEvent e)
	{
		JPanel[] PanAy = {jpN,jpS,jpE,jpW};
		String[] Location = {"North Button", "South Button","East Button","West Button"};
		
		for(int i = 0; i < Location.length ; i++)
		{
			if(e.getActionCommand().equals(Location[i]))
			{
				PanAy[i].setBackground(Color.BLUE);
			}
			
		}
		
				
	}
	
}
