import java.awt.Color;
import java.awt.Graphics;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;

public class ClrPanel extends JPanel implements ActionListener{

	private JButton cirCo = new JButton("New Circle Color Here!");
	private JButton penC = new JButton ("New Pen Color Here!");
	
	private Color cirCol = Color.BLUE;
	private Color [] bob = {Color.RED,Color.GREEN,Color.BLUE,Color.PINK,Color.YELLOW,Color.BLACK};
	private Color penCol = Color.BLUE;
	private Color [] penBob = {Color.RED,Color.GREEN,Color.BLUE,Color.PINK,Color.YELLOW,Color.BLACK};
	private int counter = 0;
	private int pcount = 0;
	
	
	public ClrPanel(){
		cirCo.addActionListener(this);
		penC.addActionListener(this);
		setLayout(new GridLayout(1,2));
		add(cirCo);
		add(penC);
	}
	
	@Override
	public void actionPerformed(ActionEvent e) {
		// TODO Auto-generated method stub
		 
		if(e.getActionCommand().trim().equalsIgnoreCase("New Circle Color Here!")){
			circdrwPanel.setDrawShapeStatus(true);
			cirCol = bob[counter%bob.length];
			counter++;
			circdrwPanel.setCircleColor(cirCol);
		}
		else if(e.getActionCommand().trim().equalsIgnoreCase("New Pen Color Here!")){
			circdrwPanel.setDrawShapeStatus(false);
			penCol = penBob[pcount%penBob.length];
			pcount++;
			circdrwPanel.setPenColor(penCol);
		}
	}

}
