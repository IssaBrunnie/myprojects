import java.awt.BorderLayout;

import javax.swing.JApplet;

public class FiOApplet extends JApplet{
	
	public void init(){
		
		backPanel Npnl = new backPanel();
		add(Npnl,BorderLayout.NORTH);
		
		WritePanel Spnl = new WritePanel();
		add(Spnl,BorderLayout.SOUTH);
		
		ClrPanel Epnl = new ClrPanel();
		add(Epnl,BorderLayout.EAST);
		
		SizePanel Wpnl = new SizePanel();
		add(Wpnl,BorderLayout.WEST);
		
		circdrwPanel Cpnl = new circdrwPanel();
		add(Cpnl, BorderLayout.CENTER);
		
	}

}
