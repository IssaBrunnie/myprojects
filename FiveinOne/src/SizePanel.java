import java.awt.Graphics;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;

public class SizePanel extends JPanel implements ActionListener{

	private JButton szB = new JButton ("Large Circle");
	private JButton szL = new JButton ("Small Circle");
	private JButton peL = new JButton ("Large Pen");
	private JButton peS = new JButton ("Small Pen");
	
	public SizePanel(){
		szB.addActionListener(this);
		szL.addActionListener(this);
		peL.addActionListener(this);
		peS.addActionListener(this);
		setLayout(new GridLayout(2,2));
		add(szB);
		add(szL);
		add(peL);
		add(peS);
	}
	
	
	@Override
	public void actionPerformed(ActionEvent e) {
		// TODO Auto-generated method stub
		if(e.getActionCommand().trim().equalsIgnoreCase("Large Circle")){
			circdrwPanel.setDrawShapeStatus(true);
			circdrwPanel.setCircleSize(50);
		}
		else if(e.getActionCommand().trim().equalsIgnoreCase("Small Circle")){
			circdrwPanel.setDrawShapeStatus(true);
			circdrwPanel.setCircleSize(10);
		}
		else if(e.getActionCommand().trim().equalsIgnoreCase("Large Pen")){
			circdrwPanel.setDrawShapeStatus(false);
			circdrwPanel.setPenSize(25);
		}
		else if(e.getActionCommand().trim().equalsIgnoreCase("Small Pen")){
			circdrwPanel.setDrawShapeStatus(false);
			circdrwPanel.setPenSize(1);
		}
		
		
		
	}

}
