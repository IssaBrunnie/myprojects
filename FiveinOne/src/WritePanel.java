import java.awt.TextField;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;

public class WritePanel extends JPanel implements ActionListener{

	private JLabel jlbset = new JLabel("Write Here!");
	private JLabel jlbchan = new JLabel("");
	private TextField jTxt = new TextField();
	private JButton Abt = new JButton("Action!!!");
	
	public WritePanel(){
		Abt.addActionListener(this);
		add(Abt);
		add(jTxt);
		add(jlbset);
		add(jlbchan);

	}
	
	@Override
	public void actionPerformed(ActionEvent e) {
		// TODO Auto-generated method stub
		jlbchan.setText("" + jTxt.getText());
		
	}

}
