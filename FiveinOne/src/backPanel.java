import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;

public class backPanel extends JPanel implements ActionListener{

	private JButton Kbt = new JButton("I am a Button!");
	private JLabel Wlb = new JLabel("Press Button for Color");
	private Color KbtCol = Color.BLACK;
	private Color[] color = {Color.GREEN, Color.YELLOW, Color.RED, Color.BLUE,Color.ORANGE};
	private int Math = 0;
	
	public backPanel(){
		Kbt.addActionListener(this);
		add(Kbt);
		add(Wlb);
	}
	
	@Override
	public void actionPerformed(ActionEvent e) {
		// TODO Auto-generated method stub
		KbtCol = color[Math%color.length];
		Math++;
		setBackground(KbtCol);
	}

}
