import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;

import javax.swing.JPanel;

public class circdrwPanel extends JPanel implements MouseListener,MouseMotionListener{

	private static boolean 		drawShape = false;
	private static Color 		circleColor = Color.BLUE;
	private static int 			circleSize = 25;
	private int 				lastX=0,lastY=0;
	private static Color		penColor = Color.BLUE;
	private static int			penSize = 10;
	
	public circdrwPanel(){
		addMouseListener(this);
		addMouseMotionListener(this);
		setBackground(Color.WHITE);
	}
	
	public static void setCircleColor(Color c){
		circleColor = c;
	}
	
	public static void setCircleSize(int r){
		circleSize = r;
	}
	
	public static Color getCircleColor(){
		return circleColor;
	}
	
	public static int getCircleSize(){
		return circleSize;
	}
	
	public static void setPenColor(Color p){
		penColor = p;
	}
	
	public static void setPenSize(int s){
		penSize = s;
	}
	
	public static Color getPenColor(){
		return penColor;
	}
	
	public static int getPenSize(){
		return penSize;
	}
	
	public void record(int x,int y){
		lastX=x;
		lastY=y;
	}
	
	public static void setDrawShapeStatus(boolean f){
		drawShape = f;
	}
	
	public static boolean getDrawShapesStatus(){
		return drawShape;
	}
	
	@Override
	public void mouseClicked(MouseEvent e) {
		// TODO Auto-generated method stub
	}

	@Override
	public void mouseEntered(MouseEvent arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void mouseExited(MouseEvent arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void mousePressed(MouseEvent e) {
		int x = e.getX();
		int y = e.getY();
		record(x,y);
		if(getDrawShapesStatus()){
			Graphics g = getGraphics();
			g.setColor(circleColor);
			g.fillOval(e.getX()-circleSize, e.getY()-circleSize, circleSize*2, circleSize*2);
		}
		
		
	}

	@Override
	public void mouseReleased(MouseEvent arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void mouseDragged(MouseEvent e) {
		int x = e.getX();
		int y = e.getY();
		Graphics g = getGraphics();
		Graphics2D g2g = (Graphics2D)g;
		//g.setColor(getPenColor());
		//g.drawLine(lastX, lastY, x, y);
		g2g.setColor(getPenColor());
		g2g.setStroke(new BasicStroke(getPenSize()));
		g2g.drawLine(lastX,lastY, x, y);
		record(x,y);
		
	}

	@Override
	public void mouseMoved(MouseEvent e) {
		// TODO Auto-generated method stub
		
	}

}
