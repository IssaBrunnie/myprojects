import java.awt.Color;
import java.awt.Graphics;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;

import javax.swing.JPanel;


public class CircleDrawer extends JPanel implements MouseListener, MouseMotionListener{

	private static boolean drawShapes = false;
	
	private static Color circleColor = Color.PINK;
	private static int circleSize = 25;
	
	private int lastX=0,lastY=0;
	
	//private static Color penColor
	
	public CircleDrawer(){
		addMouseListener(this);
		addMouseMotionListener(this);
	}
	
	
	public static void setCircleColor(Color c){
		circleColor = c;
	}
	
	public static void setCircleSize(int r){
		circleSize = r;
	}
	
	public void record(int x, int y){
		lastX = x;
		lastY = y;
	}
	
	public static boolean getDrawShapesStatus(){
		return drawShapes;
	
	}
	
	public static Color getCircleColor(){
		return circleColor;
	}
	
	public static int getCircleSize(){
		return circleSize;
	}
	
	@Override
	public void mouseClicked(MouseEvent e) {
		// TODO Auto-generated method stub
		int x = e.getX();
		int y = e.getY();
		record(x,y);
		
	}

	@Override
	public void mousePressed(MouseEvent e) {
		// TODO Auto-generated method stub
		if(drawShapes){
			Graphics g = getGraphics();
			g.setColor(circleColor);
			g.fillOval(e.getX()-circleSize, e.getY()-circleSize, circleSize*2, circleSize*2);
		}
	}

	@Override
	public void mouseReleased(MouseEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void mouseEntered(MouseEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void mouseExited(MouseEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void mouseDragged(MouseEvent e) {
		int x= e.getX();
		int y = e.getY();
		Graphics g = getGraphics();
		//g.setColor(getPenColor());
		g.setColor(getCircleColor());
		g.drawLine(lastX, lastY, x, y);
		record(x,y);
	}

	@Override
	public void mouseMoved(MouseEvent e) {
		// TODO Auto-generated method stub
		
	}

	

}
