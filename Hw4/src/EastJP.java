import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JPanel;

public class EastJP extends JPanel implements ActionListener{
	
	JButton jb = new JButton("cyan circle color");
	
	public EastJP(){
		setBackground(Color.YELLOW);
		jb.addActionListener(this);
		add(jb);
		
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		// TODO Auto-generated method stub
		if(e.getActionCommand().trim().equalsIgnoreCase("cyan circle color")){
			CircleDrawer.setCircleColor(Color.CYAN);
		}
	}
}
