import java.awt.BorderLayout;

import javax.swing.JApplet;

public class MyJApplet extends JApplet{
	
	public void inti(){
		
		EastJP jpe = new EastJP();
		add(jpe,BorderLayout.EAST);
		
		NorthJP jpn = new NorthJP();
		add(jpn,BorderLayout.NORTH);
		
		WestJP jpw = new WestJP();
		add(jpw,BorderLayout.WEST);
		
		CircleDrawer jpc = new CircleDrawer();
		add(jpc,BorderLayout.CENTER);
	}
}
