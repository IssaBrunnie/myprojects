
import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;

public class NorthJP extends JPanel implements ActionListener{
	
	private JButton jb = new JButton(" I am a button");
	private JLabel jl = new JLabel("I am a label");
	private Color jpCol = Color.BLUE;
	
	private Color [] colors = {Color.GREEN, Color.YELLOW, Color.RED, Color.BLUE, Color.PINK, Color.ORANGE};
	private int counter =0;
	
	public NorthJP(){
		jb.setText("click to color");
		jl.setText("Fun Stuff");
		jb.addActionListener(this);
		add(jb);
		add(jl);
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		// TODO Auto-generated method stub
		//setBackground(jpCol);
		jpCol = colors[counter % colors.length]; 
		counter++;
		setBackground(jpCol);
	}
	
	public void editHeading(String s){
		jl.setText(s);
	}
	
	

}

