import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JColorChooser;
import javax.swing.JPanel;

public class ColorPanel extends JPanel implements ActionListener{

	private JButton cirCo = new JButton("Shape Color Here!");
	private JButton penCo = new JButton ("Pen Color Here!");
	private JButton txtCo = new JButton ("Text color Here");
	
	public ColorPanel(){
		cirCo.addActionListener(this);
		penCo.addActionListener(this);
		txtCo.addActionListener(this);
		setLayout(new GridLayout(3,1));
		add(cirCo);
		add(penCo);
		add(txtCo);
	}
	
	@Override
	public void actionPerformed(ActionEvent e) {
		String lowActComm = e.getActionCommand().trim().toLowerCase();
		if(lowActComm.startsWith("shape")){
			DrawPanel.setShapeColor(JColorChooser.showDialog(this,"circle color",DrawPanel.getShapeColor()));
		}else if(lowActComm.startsWith("pen")){
			DrawPanel.setPenColor(JColorChooser.showDialog(this,"pen color",DrawPanel.getPenColor()));
		}else{
			DrawPanel.setTextColor(JColorChooser.showDialog(this,"textColor",DrawPanel.getPenColor()));
		}
	}
}
