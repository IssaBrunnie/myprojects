import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;

import javax.swing.JPanel;

public class DrawPanel extends JPanel implements MouseListener,MouseMotionListener{

	private static boolean 		drawShape = false;
	private static boolean		drawSqr = false;
	private static Color 		shapeColor = Color.BLUE;
	private static int 			shapeSize = 25;
	protected int 				lastX=0,lastY=0;
	private static Color		penColor = Color.BLUE;
	private static int			penSize = 5;
	private static Color		txtColor = Color.BLUE;
	
	public DrawPanel(){
		addMouseListener(this);
		addMouseMotionListener(this);
		setBackground(Color.WHITE);
	}
	
	public static void setShapeColor(Color c){
		shapeColor = c;
	}
	
	public static void setShapeSize(int r){
		shapeSize = r;
	}
	
	public static Color getShapeColor(){
		return shapeColor;
	}
	
	public static int getShapeSize(){
		return shapeSize;
	}
	
	public static void setPenColor(Color p){
		penColor = p;
	}
	
	public static void setPenSize(int s){
		penSize = s;
	}
	
	public static Color getPenColor(){
		return penColor;
	}
	
	public static int getPenSize(){
		return penSize;
	}
	
	public static void setTextColor(Color Txtco){
		txtColor = Txtco;
	}
	
	public static Color getTextColor(){
		return txtColor;
	}
	
	public void record(int x,int y){
		lastX=x;
		lastY=y;
	}
	
	public static void setSquare(boolean s){
		drawSqr = s;
	}
	
	public static boolean getSquare(){
		return drawSqr;
	}
	
	public static void setDrawShapeStatus(boolean f){
		drawShape = f;
	}
	
	public static boolean getDrawShapesStatus(){
		return drawShape;
	}
	
	@Override
	public void mouseClicked(MouseEvent e) {
		// TODO Auto-generated method stub
	}

	@Override
	public void mouseEntered(MouseEvent e) {
		// TODO Auto-generated method stub
		requestFocus();
		record(e.getX(),e.getY());
		
	}

	@Override
	public void mouseExited(MouseEvent arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void mousePressed(MouseEvent e) {
		int x = e.getX();
		int y = e.getY();
		record(x,y);
		if(getDrawShapesStatus()){
			if(getSquare()){
				Graphics g = getGraphics();
				g.setColor(shapeColor);
				g.fillRect(e.getX()-shapeSize,e.getY()-shapeSize,2*shapeSize,2*shapeSize);
			}else{
				Graphics g = getGraphics();
				g.setColor(shapeColor);
				g.fillOval(e.getX()-shapeSize, e.getY()-shapeSize, 2*shapeSize, 2*shapeSize);
			}
		}
		
		
	}

	@Override
	public void mouseReleased(MouseEvent arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void mouseDragged(MouseEvent e) {
		int x = e.getX();
		int y = e.getY();
		Graphics g = getGraphics();
		Graphics2D g2g = (Graphics2D)g;
		//g.setColor(getPenColor());
		//g.drawLine(lastX, lastY, x, y);
		g2g.setColor(getPenColor());
		g2g.setStroke(new BasicStroke(getPenSize()));
		g2g.drawLine(lastX,lastY, x, y);
		record(x,y);
		
	}

	@Override
	public void mouseMoved(MouseEvent e) {
		// TODO Auto-generated method stub
		
	}

}
