import java.awt.Color;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;

public class ErasePanel extends JPanel implements ActionListener{

	private JPanel jpErase = new JPanel();
	private JLabel ersLB = new JLabel("   --Eraser!--  ");
	private JButton ersBT = new JButton("Press!");
	
	public ErasePanel(){
		setBackground(Color.WHITE);
		jpErase.setLayout(new GridLayout(2,1));
		jpErase.setBorder(BorderFactory.createLineBorder(Color.RED));
		ersBT.addActionListener(this);
		ersBT.setToolTipText("Click Button for Eraser. Make sure to set Pen & Shape Size & Color after use.");
		jpErase.add(ersLB);
		jpErase.add(ersBT);
		jpErase.setBackground(Color.WHITE);
		add(jpErase);
	}
		
	
	@Override
	public void actionPerformed(ActionEvent e) {
		// TODO Auto-generated method stub
		DrawPanel.setPenSize(75);
		DrawPanel.setPenColor(Color.WHITE);
		DrawPanel.setShapeColor(Color.WHITE);
		
	}

}
