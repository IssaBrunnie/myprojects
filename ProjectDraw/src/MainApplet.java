import java.awt.BorderLayout;

import javax.swing.JApplet;

public class MainApplet extends JApplet{
	
	public void init(){
		
		ErasePanel Npnl = new ErasePanel();
		add(Npnl,BorderLayout.NORTH);
		
		ErrorPanel Spnl = new ErrorPanel();
		add(Spnl,BorderLayout.SOUTH);
		
		ColorPanel Epnl = new ColorPanel();
		add(Epnl,BorderLayout.EAST);
		
		WestJP_radioButtons Wpnl = new WestJP_radioButtons();
		add(Wpnl,BorderLayout.WEST);
		
		TypingandDrawing Cpnl = new TypingandDrawing();
		add(Cpnl, BorderLayout.CENTER);
		
		
	}

}
