import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.BorderFactory;
import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.JPanel;
import javax.swing.JRadioButton;

public class OnOff extends JPanel implements ActionListener{

	JPanel jpOnOff = new JPanel();
	
	public OnOff(){
		jpOnOff.setLayout(new GridLayout(5,1));
		jpOnOff.setBorder(BorderFactory.createTitledBorder("Shape ON"));
		jpOnOff.setToolTipText("Click Buttons to turn shapes On or change Shape.");
		
		ButtonGroup Ongroup = new ButtonGroup();
		
		
			JRadioButton Off = new JRadioButton();
			JRadioButton sqrOn = new JRadioButton();
			JRadioButton circOn = new JRadioButton();
			
			Off.setActionCommand("Draw Shapes Off");
			sqrOn.setActionCommand("Draw Square");
			circOn.setActionCommand("Draw Circle");
			
			Off.addActionListener(this);
			sqrOn.addActionListener(this);
			circOn.addActionListener(this);
			
			Off.setText("Draw Shapes Off");
			sqrOn.setText("Draw Square");
			circOn.setText("Draw Circle");
		
			add(Off);
			add(sqrOn);
			add(circOn);
			
			Ongroup.add(Off);
			Ongroup.add(sqrOn);
			Ongroup.add(circOn);
			
			jpOnOff.add(Off);
			jpOnOff.add(sqrOn);
			jpOnOff.add(circOn);
			
			add(jpOnOff);
			
			
		
	}
	@Override
	public void actionPerformed(ActionEvent e) {
		// TODO Auto-generated method stub
		if(e.getActionCommand().trim().equalsIgnoreCase("Draw Square")){
			DrawPanel.setDrawShapeStatus(true);
			DrawPanel.setSquare(true);
		}
		else if(e.getActionCommand().trim().equalsIgnoreCase("Draw Circle")){
			DrawPanel.setDrawShapeStatus(true);
			DrawPanel.setSquare(false);
		}
		else{
			DrawPanel.setDrawShapeStatus(false);
		}
	}

}
