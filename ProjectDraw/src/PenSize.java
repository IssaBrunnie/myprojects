

import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.BorderFactory;
import javax.swing.ButtonGroup;
import javax.swing.JPanel;
import javax.swing.JRadioButton;

public class PenSize extends JPanel implements ActionListener{
	
	JPanel jpPenSize = new JPanel();
	
	public PenSize(){
		jpPenSize.setLayout(new GridLayout(5,1));
		jpPenSize.setBorder(BorderFactory.createTitledBorder("Pen Size"));
		jpPenSize.setToolTipText("Click Buttons to switch Pen Size.");
		ButtonGroup penSizegroup = new ButtonGroup();
		
		for(int i = 1; i<6; i++){
			JRadioButton jPen = new JRadioButton();
			jPen.setActionCommand("pen size " + i);
			jPen.addActionListener(this);
			jPen.setText("pen size x " + 5*i);
			add(jPen);
			penSizegroup.add(jPen);
			jpPenSize.add(jPen);
			
		}
		add(jpPenSize);
		
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		// TODO Auto-generated method stub
		String lowActComm = e.getActionCommand().trim().toLowerCase();
		if(lowActComm.startsWith("pen")){
			int PenSize = 5 * Integer.parseInt(lowActComm.substring(lowActComm.length()-1));
			DrawPanel.setPenSize(PenSize);
		}
		
	}

}
