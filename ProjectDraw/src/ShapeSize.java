import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.BorderFactory;
import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.JPanel;
import javax.swing.JRadioButton;

public class ShapeSize extends JPanel implements ActionListener{
	
	private JPanel jpshapeSize = new JPanel();
	
	public ShapeSize (){
		jpshapeSize.setLayout(new GridLayout(5,1));
		jpshapeSize.setBorder(BorderFactory.createTitledBorder("Shape Size"));
		jpshapeSize.setToolTipText("Click Buttons to switch Size of the Shape.");
		
		ButtonGroup shapeSizegroup = new ButtonGroup();
		
		for(int i = 1; i < 6; i++){
			JRadioButton jsize = new JRadioButton();
			jsize.setActionCommand("shape size " + i);
			jsize.addActionListener(this);
			jsize.setText("shape size x " + 10*i);
			add(jsize);
			shapeSizegroup.add(jsize);
			jpshapeSize.add(jsize);
		}
		
		add(jpshapeSize);
		
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		// TODO Auto-generated method stub
		String lowActComm = e.getActionCommand().trim().toLowerCase();
		if(lowActComm.startsWith("shape")){
			int ShapeSize = 10*Integer.parseInt(lowActComm.substring(lowActComm.length()-1));
			DrawPanel.setShapeSize(ShapeSize);
		}
	}

}
