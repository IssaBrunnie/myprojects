import java.awt.Color;
import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.Graphics;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

public class TypingandDrawing extends DrawPanel implements KeyListener{

	private FontMetrics fm;
	private int fontSize = 20;
	Font font = new Font("Serif", Font.BOLD, fontSize);//default font
	
	public TypingandDrawing(){
		super();
		setLayout(null);
		setFont(font);
		fm = getFontMetrics(font);
		addKeyListener(this);
	}
	
	@Override
	public void keyPressed(KeyEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void keyReleased(KeyEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void keyTyped(KeyEvent e) {
		// TODO Auto-generated method stub
		String s = String.valueOf(e.getKeyChar());
		Graphics g = getGraphics();
		//g.setColor(DrawPanel.getTextColor());//Remember to create and call a getter and setter
		if(s.equals("\b")){
			g.setColor(Color.WHITE);
			g.drawString("█", lastX, lastY);
			record(lastX-fm.stringWidth("█"),lastY);
		}else{
			g.setColor(DrawPanel.getTextColor());
			g.drawString(s, lastX, lastY);
			record(lastX+fm.stringWidth(s),lastY);//to write one char right next to the other
		}
		
	}

}
