import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.BorderFactory;
import javax.swing.ButtonGroup;
import javax.swing.JPanel;
import javax.swing.JRadioButton;

public class WestJP_radioButtons extends JPanel {
	
	
	
	public WestJP_radioButtons(){
		setLayout(new GridLayout(4,1));
		ShapeSize shape = new ShapeSize();
		add(shape);
		PenSize pen = new PenSize();
		add(pen);
		OnOff ON = new OnOff();
		add(ON);
	}
}
